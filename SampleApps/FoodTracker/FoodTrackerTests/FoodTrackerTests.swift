//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Student on 2016-02-23.
//  Copyright © 2016 Apple Inc. All rights reserved.
//

import XCTest
@testable import FoodTracker

class FoodTrackerTests: XCTestCase {
    
    // MARK: FoodTracker Tests
    
    func testMealInitialization() {
        // Success case
        let potentialItem = Meal(name: "Newest meal", photo: nil, rating: 5)
        XCTAssertNotNil(potentialItem)
        
        // Failure case
        let noName = Meal(name: "", photo: nil, rating: 0)
        XCTAssertNil(noName)
        
        let badRating = Meal(name: "normal", photo: nil, rating: -1)
        XCTAssertNil(badRating, "Negative rating should fail initialization")
    }
    
    
}
