//
//  TableViewController.swift
//  FoodTracker
//
//  Created by Student on 2016-02-28.
//  Copyright © 2016 Apple Inc. All rights reserved.
//

import UIKit
import CoreData

class MealTableViewController: UITableViewController {
    
    // MARK: Properties
    var meals = [Meal]()
    let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Use the edit button item provided by the table view controller
        navigationItem.leftBarButtonItem = editButtonItem()
        
        if let savedMeals = fetchMeals() {
            meals += savedMeals
        }
        else {
            loadSampleMeals()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadSampleMeals() {
        let photo0 = UIImage(named: "sample000")!
        let meal0 = Meal(name: "Bullshit Meal", photo: photo0, rating: 6)!
        
        let photo1 = UIImage(named: "sample001")!
        let meal1 = Meal(name: "Unreal Meal", photo: photo1, rating: 4)!
        
        meals += [meal0, meal1]
    }

    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "MealTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! MealTableViewCell

        // Fetch the appropricate meal for the data source layout
        let meal = meals[indexPath.row]
        cell.nameLabel.text = meal.name
        cell.photoImageView.image = meal.photo
        cell.ratingControl.rating = meal.rating

        return cell
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            deleteMeal(meals[indexPath.row])
            meals.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // return false if you do not want the specified item to be editable
        return true
    }
    
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.sourceViewController as? MealViewController, meal = sourceViewController.meal {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // update existing meal
                updateMeal(meal)
                meals[selectedIndexPath.row] = meal
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            }
            else {
                // add new meal
                createMeal(meal)
                let newIndexPath = NSIndexPath(forRow: meals.count, inSection: 0)
                meals.append(meal)
                tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
            }
        }
    }

    
    
    // MARK: Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail" {
            let mealDetailViewController = segue.destinationViewController as! MealViewController
            
            // get the cell which generated this segue
            if let selectedMealCell = sender as? MealTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedMealCell)!
                let selectedMeal = meals[indexPath.row]
                mealDetailViewController.meal = selectedMeal
            }
        }
        else if segue.identifier == "AddItem" {
            print("adding new meal")
        }
    }
    
    
    
    // MARK: Core Data
    func createMeal(meal: Meal) {
        let entity =  NSEntityDescription.entityForName("Meal", inManagedObjectContext:managedContext)
        let entry = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        
        // set the value for the entry
        entry.setValue(meal.name, forKey: "name")
        entry.setValue(meal.rating, forKey: "rating")
        
        // persist the entry
        do {
            try managedContext.save()
        }
        catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func updateMeal(meal: Meal) {
        
    }
    
    func deleteMeal(meal: Meal) {
        
    }
    
    func fetchMeals() -> [Meal]? {
        let fetchRequest = NSFetchRequest(entityName: "Meal")
        var meals = [Meal]()
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
            for result in results {
                let meal = Meal(
                    name: result.valueForKey("name") as! String,
                    photo: nil,
                    rating: result.valueForKey("rating") as! Int
                )
                meals.append(meal!)
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return meals
    }
}
