________         _____                ________                 _____          
___  __ \______ ___  /_______ _       ___  __ \______ ___________  /______  __
__  / / /_  __ `/_  __/_  __ `/       __  /_/ /_  __ `/__  ___/_  __/__  / / /
_  /_/ / / /_/ / / /_  / /_/ /        _  ____/ / /_/ / _  /    / /_  _  /_/ / 
/_____/  \__,_/  \__/  \__,_/         /_/      \__,_/  /_/     \__/  _\__, /  
                                                                     /____/   ™

WIKI:
http://toxicedge.com/dataparty/wiki

Framework Library: 
https://developer.apple.com/library/ios/documentation/MultipeerConnectivity/Reference/MultipeerConnectivityFramework/

Understanding Multipeer Connectivity Framework:
http://www.appcoda.com/intro-multipeer-connectivity-framework-ios-programming/

Swift language overview
https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/GuidedTour.html

Learn Swift & iOS from beginning
https://developer.apple.com/library/ios/referencelibrary/GettingStarted/DevelopiOSAppsSwift/

Swift and Objective-C in the Same Project
https://developer.apple.com/library/ios/documentation/Swift/Conceptual/BuildingCocoaApps/MixandMatch.html

iOS & Swift Tutorial: Multipeer Connectivity
https://www.ralfebert.de/tutorials/ios-swift-multipeer-connectivity/

Ⓒ Copyright Toxicedge Inc.2016