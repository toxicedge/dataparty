//
//  DataPartyDelegate.swift
//  DataParty
//
//  Created by Chris on 2016-03-24.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation


// Public delegates for various Data Party events
@objc public protocol DataPartyDelegate : NSObjectProtocol {
    
    // Updated when device has connected/disconnected or in the process of connecting
    func connectedPeersChanged(session: DataParty, connectedPeers: [String], connectingPeers: [String])
    
    // A nearby device was found/lost
    func nearbyPeersChanged(session: DataParty, nearbyPeers: [String])
    
    // Data was successfully received from another device.
    // Needs to return true if the data was successfully saved to remove it from the list of missing files.
    func dataReceived(session: DataParty, data: DataPartyData) -> Bool
    
    // Connected state change
    func connectedStateChange(session: DataParty, isConnected: Bool)
}
