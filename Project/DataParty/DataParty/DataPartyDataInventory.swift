//
//  DataPartyDataInventory.swift
//  DataParty
//
//  Created by Chris on 2016-03-15.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation

// Class for informing other Data Party clients of available data for transfer
internal class DataPartyDataInventory: NSObject, NSCoding {
    
    // MARK: Properties
    
    private struct PropertyKey
    {
        static let items = "items"
        static let dateCreated = "dateCreated"
        static let changesImminent = "changesImminent"
    }
    
    var items: Set<DataPartyDataIdentifier> = Set<DataPartyDataIdentifier>()
    private(set) var dateCreated: NSDate = NSDate()
    private(set) var dateReceived: NSDate = NSDate()

    
    // MARK: Constructor
    
    override init()
    {
        super.init()
    }
    
    
    // MARK: Encode/Decode
    
    internal required init(coder aDecoder: NSCoder)
    {
        self.items = aDecoder.decodeObjectForKey(PropertyKey.items) as! Set<DataPartyDataIdentifier>
        self.dateCreated = aDecoder.decodeObjectForKey(PropertyKey.dateCreated) as! NSDate
        super.init()
    }
    
    internal func encodeWithCoder(aCoder: NSCoder)
    {
        aCoder.encodeObject(self.items, forKey: PropertyKey.items)
        aCoder.encodeObject(self.dateCreated, forKey: PropertyKey.dateCreated)
    }
    
    
}