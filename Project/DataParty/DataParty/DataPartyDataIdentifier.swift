//
//  DataPartyDataIdentifier.swift
//  DataParty
//
//  Created by Chris on 2016-03-15.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation

// Unique identifier for data comparison
// Composed of dataType and the uniqueId of the data
internal class DataPartyDataIdentifier: NSObject, NSCoding {
    
    private struct PropertyKey {
        static let dataType = "dataType"
        static let dataHash = "dataHash"
    }
    
    let dataType: String
    let dataHash: NSData
    
    init(dataType: String, dataHash: NSData){
        self.dataType = dataType
        self.dataHash = dataHash
    }
    
    internal required init(coder aDecoder: NSCoder){
        self.dataType = aDecoder.decodeObjectForKey(PropertyKey.dataType) as! String
        self.dataHash = aDecoder.decodeObjectForKey(PropertyKey.dataHash) as! NSData
        super.init()
    }
    
    internal func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(self.dataType, forKey: PropertyKey.dataType)
        aCoder.encodeObject(self.dataHash, forKey: PropertyKey.dataHash)
    }
    
    override func isEqual(object: AnyObject?) -> Bool
    {
        var result = false;
        
        if let object = object as? DataPartyDataIdentifier
        {
            if self == object
            {
                result = true
            }
        }
        
        return result
    }
    
    override var hash: Int
    {
        return dataType.hash ^ dataHash.hash
    }
    
}

// Operator overload
func ==(left: DataPartyDataIdentifier, right: DataPartyDataIdentifier) -> Bool
{
    return (left.dataType == right.dataType) && (left.dataHash == right.dataHash)
}

