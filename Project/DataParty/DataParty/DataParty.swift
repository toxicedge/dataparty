//
//  DataParty.swift
//  DataParty
//  Class encapsulates all the functionality of the Data Party framework
//
//  Created by Chris on 2016-02-23.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation
import MultipeerConnectivity


public class DataParty: NSObject {
    

    // PUBLIC
    
    public var nearbyPeerNames: [String] {
        get {
            var names = [String]()
            self.nearbyPeers.forEach {(i) in
                names.append(i.displayName)
            }
            return names
        }
    }
    
    public var connectingPeerNames: [String] {
        get {
            var names = [String]()
            self.connectingPeers.forEach {(i) in
                names.append(i.displayName)
            }
            return names
        }
    }
    
    public var connectedPeerNames: [String] {
        get {
            return session.connectedPeers.map({$0.displayName})
        }
    }
    
    public var deviceId: String {
        get {
            return UIDevice.currentDevice().identifierForVendor!.UUIDString
        }
    }
    
    public let sessionId : String
    public weak var delegate: DataPartyDelegate?
    public private(set) var isStarted: Bool = false
    public private(set) var isConnected: Bool = false

    // PRIVATE

    // Multipeer Connectivity Framework
    public var sessionConnectTimeout: NSTimeInterval = 8
    public var dataRefreshInterval: NSTimeInterval = 10
    public var maxDataPackSize: Int = 1000000
    private var connectingPeers = Set<MCPeerID>()
    private var nearbyPeers = Set<MCPeerID>()
    private let multipeersessionUsername = MCPeerID(displayName: UIDevice.currentDevice().name)
    private var serviceAdvertiser : MCNearbyServiceAdvertiser
    private var serviceBrowser : MCNearbyServiceBrowser
    private lazy var session: MCSession =
    {
        let session = MCSession(peer: self.multipeersessionUsername, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.Required)
        session.delegate = self
        return session
    }()
    private var invitationAcceptTime: NSDate? = nil
    private var invitationAcceptPeer: MCPeerID? = nil
    private var invitationAcceptInvitationMessage: DataPartyInvitationMessage? = nil
    
    // Data Management
    private var items = Set<DataPartyData>()
    private var missingItems = Set<DataPartyDataIdentifier>()
    private var peerInventories = [MCPeerID:DataPartyDataInventory]()
    private var itemWhereabouts = [DataPartyDataIdentifier:Set<MCPeerID>]()
    private var lastDataReceived: NSDate? = nil
    
    // Timers
    private var inviteNearbyPeersTimer = NSTimer()
    private var verifyConnectionStateTimer = NSTimer()
    private var requestMissingDataTimer = NSTimer()
    
    

    
    
    // Constructor
    // Session ID must be specified for Data Party to work
    public init(sessionId: String, autostart: Bool = true)
    {
        self.sessionId = sessionId
        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: multipeersessionUsername, discoveryInfo: nil, serviceType: self.sessionId)
        self.serviceBrowser = MCNearbyServiceBrowser(peer: multipeersessionUsername, serviceType: self.sessionId)
        
        super.init()
        
        self.serviceAdvertiser.delegate = self
        self.serviceBrowser.delegate = self
        
        if(autostart)
        {
            start()
        }
        
        // Timers
        inviteNearbyPeersTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector:
            #selector(DataParty.inviteNearbyPeers), userInfo: nil, repeats: true)
        
        verifyConnectionStateTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector:
            #selector(DataParty.updatedConnectedState), userInfo: nil, repeats: true)
        
        requestMissingDataTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector:
            #selector(DataParty.refreshDataRequests), userInfo: nil, repeats: true)
    }
    
    // Destructor
    deinit
    {
        stop()
    }


    
    // Start broadcasting and connecting to peers
    public func start()
    {
        if(!isStarted)
        {
            self.serviceAdvertiser.startAdvertisingPeer()
            self.serviceBrowser.startBrowsingForPeers()
            self.isStarted = true
            NSLog("%@", "DataParty '\(sessionId)' Started. Device ID: '\(deviceId)'")
        }
    }
    
    // Stop broadcasting and disconnect from clients
    public func stop()
    {
        if(isStarted)
        {
            self.session.disconnect()
            self.serviceAdvertiser.stopAdvertisingPeer()
            self.serviceBrowser.stopBrowsingForPeers()
            self.connectingPeers.removeAll()
            self.nearbyPeers.removeAll()
            self.missingItems.removeAll()
            self.peerInventories.removeAll()
            self.isStarted = false
            NSLog("%@", "DataParty '\(sessionId)' Stopped")
        }
    }
    
    
    public func restartBrowser()
    {
        self.connectingPeers.removeAll()
        self.nearbyPeers.removeAll()
        self.serviceBrowser = MCNearbyServiceBrowser(peer: multipeersessionUsername, serviceType: self.sessionId)
        self.serviceBrowser.delegate = self
    }
    
    public func restartAdvertiser()
    {
        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: multipeersessionUsername, discoveryInfo: nil, serviceType: self.sessionId)
        self.serviceAdvertiser.delegate = self
    }
    
    // Update 'isConnected'
    // Connection occurs when there are peers in the session
    public func updatedConnectedState()
    {
        let initialState = self.isConnected
        
        // 'connected' if there is more than one peer in our session
        self.isConnected = self.session.connectedPeers.count > 0
        
        // Update network indicator
        UIApplication.sharedApplication().networkActivityIndicatorVisible = self.isConnected

        // Notify observers if state change
        if initialState != self.isConnected
        {
            NSLog("%@", "DataParty '\(sessionId)' isConnected: \(self.isConnected)")
            self.delegate?.connectedStateChange(self, isConnected: self.isConnected)
        }
    }
}





// MARK: Public Data Functions

extension DataParty
{
    // Add data to be synchronized across peers
    // Returns true if data was added
    public func addData(data: DataPartyData) -> Bool
    {
        NSLog("%@", "DataParty '\(sessionId)' Added data: \(data.dataType)")
        var result = false
        
        if !self.items.contains(data)
        {
            if !DataPartyDataRegistry.isDataIgnored(data)
            {
                // Inject details
                if(DataPartyDataRegistry.isDataRegistered(data)){
                    DataPartyDataRegistry.injectDataDetails(data)
                }
                else{
                    DataPartyDataRegistry.registerData(data)
                }
                
                self.items.insert(data)
                result = true
                
                updateMissingItems()
                
                // Update peers with new inventory
                sendDataInventory(self.session.connectedPeers)
            }
            else
            {
                NSLog("%@", "DataParty '\(sessionId)' Ignoring data: \(data.dataType)")
            }
        }
        else
        {
            NSLog("%@", "DataParty '\(sessionId)' Ignoring duplicate item: '\(data.dataType)'")
        }
        
        return result
    }
    
    // Remove data from synchronization process
    public func removeData(data: DataPartyData)
    {
        NSLog("%@", "DataParty '\(sessionId)' Removing data: \(data.dataType)")
        self.items.remove(data)
        updateMissingItems()
    }
    
    // Ignore specified data object
    public func ignoreData(data: DataPartyData)
    {
        NSLog("%@", "DataParty '\(sessionId)' Ignoring data: \(data.dataType)")
        DataPartyDataRegistry.addDataToIgnoreList(data);
        removeData(data);
    }
    
    // Remove all data from the synchronization list so that it can be re-added
    // This will disable Data Party to prevent auto synchronization
    // Enable Data Party again after you are done re-entering data
    public func stopAndFlushData()
    {
        stop()
        self.items.removeAll()
        NSLog("%@", "DataParty '\(sessionId)' Flushed all data. Note: Data Party needs to be started again")
    }
}






// MARK: Message Processing

extension DataParty
{
    // Message handling switch
    // Identifies the type of message that has been received and determines what needs to be done
    func processMessage(message: DataPartyMessage, peerId: MCPeerID)
    {
        NSLog("%@", "DataParty '\(self.sessionId)' Processing '\(message.messageType.rawValue)' message sent by \(peerId.displayName)")
        
        switch message.messageType
        {
            
        case DataPartyMessageType.DataInventory:
            processDataInventoryMessage(peerId, error: message.payload.error, dataInventory: message.payload.dataInventory)
            
        case DataPartyMessageType.DataInventoryRequest:
            processDataInventoryMessage(peerId, error: message.payload.error, dataInventory: message.payload.dataInventory)
            processDataInventoryRequestMessage(peerId)
            
        case DataPartyMessageType.DataRequest:
            processDataInventoryMessage(peerId, error: message.payload.error, dataInventory: message.payload.dataInventory)
            processDataRequestMessage(peerId, dataIdentifiers: message.payload.dataIdentifiers)
            
        case DataPartyMessageType.Data:
            processDataInventoryMessage(peerId, error: message.payload.error, dataInventory: message.payload.dataInventory)
            processDataMessage(peerId, error: message.payload.error, data: message.payload.data, dataIdentifiers: message.payload.dataIdentifiers);
        
        case DataPartyMessageType.DataPack:
            processDataInventoryMessage(peerId, error: message.payload.error, dataInventory: message.payload.dataInventory)
            processDataPackMessage(peerId, error: message.payload.error, dataPack: message.payload.dataPack, dataIdentifiers: message.payload.dataIdentifiers);
        
        }
    }
    
    // Process data inventory received from a peer
    private func processDataInventoryRequestMessage(peerId: MCPeerID)
    {
        sendDataInventory([peerId])
    }
    
    // Process a data request
    private func processDataRequestMessage(peerId: MCPeerID, dataIdentifiers: Set<DataPartyDataIdentifier>?)
    {
        if dataIdentifiers != nil && dataIdentifiers?.count > 0
        {
            if dataIdentifiers?.count > 1
            {
                sendData([peerId], dataIdentifier: dataIdentifiers?.first)
            }
            else
            {
                sendDataPack([peerId], dataIdentifiers: dataIdentifiers)
            }
        }
        else
        {
            NSLog("%@", "DataParty '\(self.sessionId)' Data request from \(peerId.displayName) doesn't contain any requested items")
        }
    }
    
    // Process received data
    private func processDataMessage(mcPeerId: MCPeerID, error: DataPartyErrorType, data: DataPartyData?, dataIdentifiers: Set<DataPartyDataIdentifier>?)
    {
        self.lastDataReceived = NSDate()
        
        if(error == DataPartyErrorType.None)
        {
            if (data != nil)
            {
                if(addData(data!))
                {
                    NSLog("%@", "DataParty '\(self.sessionId)' Notifying application of new data deceived")
                    delegate?.dataReceived(self, data: data!)
                    sendDataInventory(self.session.connectedPeers)
                }
                else
                {
                    NSLog("%@", "DataParty '\(self.sessionId)' Received data that already exists")
                }
            }
            else
            {
                NSLog("%@", "DataParty '\(self.sessionId)' Data message received is nil")
            }
        }
        else if error == DataPartyErrorType.DataUnavailable && dataIdentifiers != nil && dataIdentifiers?.count > 0
        {
            for dataIdentifier in dataIdentifiers!
            {
                NSLog("%@", "DataParty '\(self.sessionId)' Requested data '\(dataIdentifier.dataType)' is not available from: \(mcPeerId.displayName)")
                peerInventories[mcPeerId]?.items.remove(dataIdentifier)
                updateMissingItems()
                updateItemWhereabouts()
            }
        }
        else
        {
            NSLog("%@", "DataParty '\(self.sessionId)' Error processing data message: \(error.rawValue)")
        }
    }
    
    // Process received data pack
    private func processDataPackMessage(mcPeerId: MCPeerID, error: DataPartyErrorType, dataPack: Set<DataPartyData>?, dataIdentifiers: Set<DataPartyDataIdentifier>?)
    {
        self.lastDataReceived = NSDate()
        
        if(error == DataPartyErrorType.None)
        {
            if (dataPack != nil && dataPack?.count > 0)
            {
                for data in dataPack!
                {
                    if(addData(data))
                    {
                        NSLog("%@", "DataParty '\(self.sessionId)' Notifying application of new data deceived")
                        delegate?.dataReceived(self, data: data)
                        sendDataInventory(self.session.connectedPeers)
                    }
                    else
                    {
                        NSLog("%@", "DataParty '\(self.sessionId)' Received data that already exists")
                    }
                }
            }
            else
            {
                NSLog("%@", "DataParty '\(self.sessionId)' Data pack received is nil or empty")
            }
        }
        else if error == DataPartyErrorType.DataUnavailable && dataIdentifiers != nil && dataIdentifiers?.count > 0
        {
            for dataIdentifier in dataIdentifiers!
            {
                NSLog("%@", "DataParty '\(self.sessionId)' Requested data '\(dataIdentifier.dataType)' is not available from: \(mcPeerId.displayName)")
                peerInventories[mcPeerId]?.items.remove(dataIdentifier)
                updateMissingItems()
                updateItemWhereabouts()
            }
        }
        else
        {
            NSLog("%@", "DataParty '\(self.sessionId)' Error processing data message: \(error.rawValue)")
        }
    }
    
    // Process inventory list that was reveived from a peer
    private func processDataInventoryMessage(mcPeerId: MCPeerID, error: DataPartyErrorType, dataInventory: DataPartyDataInventory?)
    {
        if(error == DataPartyErrorType.None)
        {
            if (dataInventory != nil)
            {
                NSLog("%@", "DataParty '\(self.sessionId)' Data inventory [\(dataInventory!.items.count)] received from \(mcPeerId.displayName)")
                
                self.peerInventories[mcPeerId] = dataInventory
                updateMissingItems()
                updateItemWhereabouts()
                requestAllData()
            }
            else
            {
                NSLog("%@", "DataParty '\(self.sessionId)' Data inventory received from \(mcPeerId.displayName) is nil")
            }
        }
        else
        {
            NSLog("%@", "DataParty '\(self.sessionId)' Unable to process data inventory from \(mcPeerId.displayName): \(error.rawValue)")
        }
    }
}







// MARK: Data Management

extension DataParty
{
    // Remove traces of a peer
    // Should be done when a peer leaves the network
    private func removeTracesOfPeer(peerId: MCPeerID)
    {
        self.peerInventories.removeValueForKey(peerId)
        self.nearbyPeers.remove(peerId)
        updateItemWhereabouts()
        updateMissingItems()
    }
    
    // Update the missing items list
    private func updateMissingItems()
    {
        self.missingItems.removeAll()
        
        for peerInventory in self.peerInventories
        {
            peerInventory.1.items.forEach({(dataIdentifier) in
                if(!DataPartyDataRegistry.isDataIgnored(dataIdentifier) &&
                    !self.items.contains({(i) in i.dataIdentifier == dataIdentifier}))
                {
                    // Insert into missingItems if data is not ignored
                    // and data does not already exist in items
                    self.missingItems.insert(dataIdentifier)
                }
            })
        }

        NSLog("%@", "DataParty '\(sessionId)' Updated missing items list. \(self.missingItems.count) item(s) required")
    }
    
    // Update item whereabouts list
    private func updateItemWhereabouts()
    {
        self.itemWhereabouts.removeAll()
        
        // Iterate through peers
        for item in self.peerInventories
        {
            let peerId = item.0
            let dataInventory = item.1
            
            // Iterate through each inventory
            for dataIdentifier in dataInventory.items
            {
                if(self.itemWhereabouts[dataIdentifier] != nil)
                {
                    // add item to whereabouts
                    self.itemWhereabouts[dataIdentifier]!.insert(peerId)
                }
                else
                {
                    // Initialize set and insert item
                    self.itemWhereabouts[dataIdentifier] = Set<MCPeerID>()
                    self.itemWhereabouts[dataIdentifier]!.insert(peerId)
                }
            }
        }
        
        NSLog("%@", "DataParty '\(sessionId)' Updated item whereabouts list")
    }
    
    // Returns a data party inventoy of items that have been marked sharable
    private func getSharableInventory() -> DataPartyDataInventory
    {
        let inventory = DataPartyDataInventory();
        for data in self.items
        {
            if data.disableSync == false
            {
                inventory.items.insert(data.dataIdentifier)
            }
        }
        
        return inventory
    }
}






// MARK: Data Transmission

extension DataParty
{
    // Request data inventory from peer(s)
    // Returns true if data was sent
    private func requestDataInventory(peers: [MCPeerID]) -> Bool
    {
        if self.isStarted && peers.count > 0
        {
            NSLog("%@", "DataParty '\(sessionId)' Requesting inventory from \(peers.map({$0.displayName}))")
            
            // Initialize message
            let message = DataPartyMessage(messageType: DataPartyMessageType.DataInventoryRequest)
            
            // Encode message
            let messageData = NSKeyedArchiver.archivedDataWithRootObject(message)
            
            // Send message
            do {
                try self.session.sendData(messageData, toPeers: peers, withMode: MCSessionSendDataMode.Reliable)
            } catch let error {
                NSLog("%@", "DataParty '\(sessionId)' Error requesting inventory: \(error)")
                return false
            }
            return true
        }
        return false
    }
    
    // Request all missing data from all connected peers if no data has been received in a while
    // This is to be called from a Timer
    public func refreshDataRequests()
    {
        var timeSinceLastDataReceived = Double.infinity;
        if let lastDataTime = self.lastDataReceived?.timeIntervalSinceNow {
            timeSinceLastDataReceived = abs(lastDataTime)
        }
        
        if self.lastDataReceived == nil || timeSinceLastDataReceived > dataRefreshInterval
        {
            requestAllData()
        }
    }
    
    // Request all missing data from all connected peers
    private func requestAllData(multipleRequestsFromSamePeer: Bool = false)
    {
        if self.isStarted
        {
            var requestedPeers = Set<MCPeerID>()
            
            for item in self.missingItems
            {
                if let peers = self.itemWhereabouts[item]
                {
                    let possiblePeers = peers.filter({(peer) in
                        self.session.connectedPeers.contains(peer) &&
                        !requestedPeers.contains(peer)
                    })
                    
                    if(possiblePeers.count > 0)
                    {
                        let rand = Int(arc4random_uniform(UInt32(possiblePeers.count)))
                        
                        requestData(item, peers: [possiblePeers[rand]])
                        
                        if(!multipleRequestsFromSamePeer)
                        {
                            requestedPeers.insert(possiblePeers[rand])
                        }
                    }
                }
            }
        }
    }
    
    // Request data from peer(s)
    // Returns true if data was sent
    private func requestData(dataIdentifier: DataPartyDataIdentifier, peers: [MCPeerID]) -> Bool
    {
        if self.isStarted && peers.count > 0
        {
            NSLog("%@", "DataParty '\(sessionId)' Requesting data from \(peers.map({$0.displayName}))")
            
            // Initialize message
            let message = DataPartyMessage(messageType: DataPartyMessageType.DataRequest)
            message.payload.dataIdentifiers = [dataIdentifier]
            message.payload.dataInventory = getSharableInventory()
            
            // Encode message
            let messageData = NSKeyedArchiver.archivedDataWithRootObject(message)

            // Send message
            do {
                try self.session.sendData(messageData, toPeers: peers, withMode: MCSessionSendDataMode.Reliable)
            } catch let error {
                NSLog("%@", "DataParty '\(sessionId)' Error requesting data (\(dataIdentifier.dataType)) from \(peers.map({$0.displayName})): \(error)")
                return false
            }
            return true
        }
        return false
    }
    
    // Send  data inventory to peer(s)
    // Returns true if data was sent
    private func sendDataInventory(peers: [MCPeerID]) -> Bool
    {
        if self.isStarted && peers.count > 0
        {
            NSLog("%@", "DataParty '\(sessionId)' Sending data inventory to \(peers.map({$0.displayName}))")
            
            // Initialize message
            let message = DataPartyMessage(messageType: DataPartyMessageType.DataInventory)
            message.payload.dataInventory = getSharableInventory()

            // Encode message
            let messageData = NSKeyedArchiver.archivedDataWithRootObject(message)
            
            // Send message
            do {
                try self.session.sendData(messageData, toPeers: peers, withMode: MCSessionSendDataMode.Reliable)
            } catch let error {
                NSLog("%@", "DataParty '\(sessionId)' Error sending data inventory: \(error)")
                return false
            }
            
            return true
        }
        
        return false
    }

    // Send data to peer(s)
    // Data is sent along with an up to date inventory
    // Returns true if data was sent
    private func sendData(peers: [MCPeerID], dataIdentifier: DataPartyDataIdentifier?) -> Bool
    {
        if self.isStarted && peers.count > 0
        {
            if dataIdentifier != nil
            {
                NSLog("%@", "DataParty '\(sessionId)' Sending item \(dataIdentifier!.dataType) to \(peers.map({$0.displayName}))")
                
                // Initialize message
                let message = DataPartyMessage(messageType: DataPartyMessageType.Data)
                message.payload.dataIdentifiers = [dataIdentifier!]
                message.payload.dataInventory = getSharableInventory()
                
                let i = self.items.indexOf({(data) in data.dataIdentifier == dataIdentifier })
                
                if (i != nil)
                {
                    message.payload.data = self.items[i!]
                }
                else
                {
                    NSLog("%@", "DataParty '\(sessionId)' Error sending item: requested data does not exist")
                    return false
                }
                
                // Encode message
                let messageData = NSKeyedArchiver.archivedDataWithRootObject(message)
                
                // Send message
                do {
                    try self.session.sendData(messageData, toPeers: peers, withMode: MCSessionSendDataMode.Reliable)
                } catch let error {
                    NSLog("%@", "DataParty '\(sessionId)') Error sending item: \(error)")
                    return false
                }
                return true
            }
            else
            {
                NSLog("%@", "DataParty '\(self.sessionId)' No identifier specified in data request")
            }
        }
        return false
    }
    
    // Send data to peer(s)
    // Data is sent along with an up to date inventory
    // Returns true if data was sent
    private func sendDataPack(peers: [MCPeerID], dataIdentifiers: Set<DataPartyDataIdentifier>?) -> Bool
    {
        if self.isStarted && peers.count > 0
        {
            if dataIdentifiers != nil && dataIdentifiers?.count > 0
            {
                NSLog("%@", "DataParty '\(sessionId)' Sending item(s) \(dataIdentifiers!.map({$0.dataType})) to \(peers.map({$0.displayName}))")
                
                // Initialize message
                let message = DataPartyMessage(messageType: DataPartyMessageType.DataPack)
                message.payload.dataInventory = getSharableInventory()
                var missingItems = Set<DataPartyDataIdentifier>()
                var messageSize = 0;
                
                // Build up message until max message size is reached
                for dataIdentifier in dataIdentifiers!
                {
                    if let i = self.items.indexOf({(data) in data.dataIdentifier == dataIdentifier })
                    {
                        let itemSize = NSKeyedArchiver.archivedDataWithRootObject(self.items[i]).length
                        
                        if maxDataPackSize > (itemSize + messageSize)
                        {
                            message.payload.dataPack?.insert(self.items[i])
                            message.payload.dataIdentifiers?.insert(dataIdentifier)
                            messageSize += itemSize
                        }
                        else
                        {
                            break
                        }
                    }
                    else
                    {
                        // Do something with this later
                        missingItems.insert(dataIdentifier)
                    }
                }
                
                // SEND DATA PACK
                // Encode message
                let messageData = NSKeyedArchiver.archivedDataWithRootObject(message)
                
                // Send message
                do {
                    try self.session.sendData(messageData, toPeers: peers, withMode: MCSessionSendDataMode.Reliable)
                } catch let error {
                    NSLog("%@", "DataParty '\(sessionId)') Error sending item: \(error)")
                    return false
                }
                
                return true
            }
            else
            {
                NSLog("%@", "DataParty '\(self.sessionId)' Send Data Pack: 'dataIdentifiers is' nil or empty")
            }
        }
        return false
    }
}







// MARK: MC Session Management

extension DataParty
{
    // Invite nearby peers to session
    // This function is to be called from a timer
    public func inviteNearbyPeers()
    {
        if(self.isStarted)
        {
            self.nearbyPeers.forEach { (peerId) in
                invitePeer(peerId)
            }
        }
    }
    
    // Decide whether to accept a peer invite
    private func shouldIAcceptThisSessionInvitation(peerId: MCPeerID, invitation: DataPartyInvitationMessage) -> Bool
    {
        var result = false
        
        // Consider the invite if they are not an existing peer
        if !self.session.connectedPeers.contains(peerId)
        {
            var timeSinceLastInvitationAccept = Double.infinity;
            if let acceptTime = invitationAcceptTime?.timeIntervalSinceNow {
                timeSinceLastInvitationAccept = abs(acceptTime)
            }
            
            // Check if an invite is already being processed
            if timeSinceLastInvitationAccept < sessionConnectTimeout
            {
                NSLog("%@", "DataParty '\(sessionId)' Note: Already accepting invite \(timeSinceLastInvitationAccept)")
                
                if invitationAcceptInvitationMessage!.connectedPeers.contains(peerId)
                {
                    // Determine whether to cancel the current connect process
                    if invitationAcceptPeer != peerId
                    {
                        if invitation.connectedPeersCount > invitationAcceptInvitationMessage?.connectedPeersCount
                        {
                            // Yes - new invite has more peers
                            NSLog("%@", "DataParty '\(sessionId)' New invitation has more peers than current invitation (\(invitation.connectedPeersCount) vs. \(invitationAcceptInvitationMessage?.connectedPeersCount))")
                            result = true
                        }
                        else if invitation.connectedPeersCount == invitationAcceptInvitationMessage?.connectedPeersCount
                        {
                            if invitation.deviceId.caseInsensitiveCompare((invitationAcceptInvitationMessage?.deviceId)!) == NSComparisonResult.OrderedAscending
                            {
                                // Yes - new invite has same peer count but higher device id
                                NSLog("%@", "DataParty '\(sessionId)' New invitation has same number of peers (\(invitation.connectedPeersCount)) but ranks higher (\(invitation.deviceId) vs. \(self.deviceId))")
                                result = true
                            }
                        }
                    }
                }
                else
                {
                    NSLog("%@", "DataParty '\(sessionId)' New invitation is from the same session as the current accepted invitation")
                }
            }
            else if invitation.connectedPeersCount > session.connectedPeers.count
            {
                // Yes - if they have more peers
                NSLog("%@", "DataParty '\(sessionId)' Invitation has more peers (\(invitation.connectedPeersCount) vs. \(self.session.connectedPeers.count))")
                result = true
            }
            else if invitation.connectedPeersCount == session.connectedPeers.count
            {
                if self.deviceId.caseInsensitiveCompare(invitation.deviceId) == NSComparisonResult.OrderedAscending
                {
                    // Yes - same peer count but higher device id
                    NSLog("%@", "DataParty '\(sessionId)' Invitation has same number of peers (\(invitation.connectedPeersCount)) but ranks higher (\(invitation.deviceId) vs. \(self.deviceId))")
                    result = true
                }
            }
        }
        
        NSLog("%@", "DataParty '\(sessionId)' Should I connect to \(peerId.displayName)?: \(result)")
        return result
    }
    
    // Send invitation to a peer if they are not connected or attempting to connect
    private func invitePeer(peerId: MCPeerID)
    {
        // Are we started and not already connected/connecting to the desired peer?
        if self.isStarted && !self.connectingPeers.contains(peerId) && !self.session.connectedPeers.contains(peerId)
        {
            // Are we currently in the process of connecting to another peer?
            var timeSinceLastInvitationAccept = Double.infinity;
            if let acceptTime = invitationAcceptTime?.timeIntervalSinceNow {
                timeSinceLastInvitationAccept = abs(acceptTime)
            }
            
            // Dont invite if in the process of connecting to peer
            if timeSinceLastInvitationAccept > sessionConnectTimeout
            {
                let invitationMessage = DataPartyInvitationMessage(connectedPeers: self.session.connectedPeers)
                let invitationMessageBytes = NSKeyedArchiver.archivedDataWithRootObject(invitationMessage)
                
                NSLog("%@", "DataParty '\(sessionId)' Inviting \(peerId.displayName) to session")
                
                self.serviceBrowser.invitePeer(peerId, toSession: self.session, withContext: invitationMessageBytes, timeout: self.sessionConnectTimeout)
                
            }
        }
    }
}



































// MARK: MCNearbyServiceAdvertiserDelegate
// The advertiser broadcasts the session for other browsers to find
// The advertiser also manages incoming invitation requests

extension DataParty : MCNearbyServiceAdvertiserDelegate
{
    // Incoming invitation request.  Call the invitationHandler block with YES
    // and a valid session to connect the inviting peer to the session.
    public func advertiser(advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: NSData?, invitationHandler: ((Bool, MCSession) -> Void))
    {
        NSLog("%@", "DataParty '\(sessionId)' Received invitation from \(peerID.displayName)")
        
        if let invitation = NSKeyedUnarchiver.unarchiveObjectWithData(context!) as? DataPartyInvitationMessage
        {
            if shouldIAcceptThisSessionInvitation(peerID, invitation: invitation)
            {
                NSLog("%@", "DataParty '\(sessionId)' Connecting to \(peerID.displayName) session")
                
                // Cancel current connection attempt (if applicable)
                var timeSinceLastInvitationAccept = Double.infinity;
                if let acceptTime = invitationAcceptTime?.timeIntervalSinceNow {
                    timeSinceLastInvitationAccept = abs(acceptTime)
                }
                
                if timeSinceLastInvitationAccept < sessionConnectTimeout
                {
                    NSLog("%@", "DataParty '\(sessionId)' Cancelling previous connection attempt to \(self.invitationAcceptPeer!.displayName) session")
                    self.session.cancelConnectPeer(invitationAcceptPeer!)
                }
                
                if session.connectedPeers.count > 0
                {
                    NSLog("%@", "DataParty '\(sessionId)' Disconnecting from current session (\(self.session.connectedPeers.count))")
                    session.disconnect()
                }
                
                // Update invitation info and connect
                self.invitationAcceptTime = NSDate()
                self.invitationAcceptPeer = peerID
                self.invitationAcceptInvitationMessage = invitation
                invitationHandler(true, self.session)
            }
        }
        else
        {
            NSLog("%@", "DataParty '\(sessionId)' Error decoding invitation message from \(peerID.displayName)")
        }
    }
    
    // Advertising did not start due to an error.
    public func advertiser(advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: NSError)
    {
        NSLog("%@", "DataParty '\(self.sessionId)' Error starting advertiser: \(error)")
    }
}







// MARK: MCNearbyServiceBrowserDelegate
// The Service browser looks for peers in the area

extension DataParty : MCNearbyServiceBrowserDelegate
{
    // Found a nearby advertising peer.
    public func browser(browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?)
    {
        NSLog("%@", "DataParty '\(sessionId)' Found \(peerID.displayName)")
        
        // Update nearby peers list
        if !self.nearbyPeers.contains(peerID)
        {
            self.nearbyPeers.insert(peerID)
            self.delegate?.nearbyPeersChanged(self, nearbyPeers: self.nearbyPeerNames)
        }
        
        invitePeer(peerID)
    }
    
    // A nearby peer has stopped advertising.
    public func browser(browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID)
    {
        NSLog("%@", "DataParty '\(sessionId)' Lost \(peerID.displayName)")
        
        if self.nearbyPeers.contains(peerID)
        {
            removeTracesOfPeer(peerID)
            self.delegate?.nearbyPeersChanged(self, nearbyPeers: self.nearbyPeerNames)
        }
    }
    
    // Browsing did not start due to an error.
    public func browser(browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: NSError)
    {
        NSLog("%@", "DataParty '\(sessionId)' Error starting browser: \(error)")
    }
}





// MARK: MCSessionState

extension MCSessionState {
    
    func stringValue() -> String {
        switch(self) {
        case .NotConnected: return "NotConnected"
        case .Connecting: return "Connecting"
        case .Connected: return "Connected"
        }
    }
}





// MARK: MCSessionDelegate

extension DataParty : MCSessionDelegate {
    
    // Remote peer changed state.
    public func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState)
    {
        NSLog("%@", "DataParty '\(sessionId)' \(peerID.displayName): \(state.stringValue())")
        
        switch state
        {
        case MCSessionState.NotConnected:
            
            self.connectingPeers.remove(peerID)
            
            // Reset peer invitation time if that was the peer we were connecting to
            if(peerID == invitationAcceptPeer)
            {
                invitationAcceptTime = nil
            }

        case MCSessionState.Connecting:
            self.connectingPeers.insert(peerID)

        case MCSessionState.Connected:
            self.connectingPeers.remove(peerID)
            
            sendDataInventory([peerID])
        }
        
        NSLog("%@", "DataParty '\(sessionId)' Connected peers: \(self.session.connectedPeers.count)")
        
        self.delegate?.connectedPeersChanged(self, connectedPeers: self.connectedPeerNames, connectingPeers: self.connectingPeerNames)
        
        updatedConnectedState()
    }
    
    // Received data from remote peer.
    public func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID)
    {
        NSLog("%@", "DataParty '\(sessionId)' Received data (\(data.length) bytes) from \(peerID.displayName)")
        
        if let message = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? DataPartyMessage {
            processMessage(message, peerId: peerID)
        } else {
            NSLog("%@", "DataParty '\(sessionId)' Unable to decode DataPartyMessage object. Data may be corrupt")
        }
    }
    
    // Received a byte stream from remote peer.
    // NOT USED
    public func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID)
    {
        NSLog("%@", "DataParty '\(sessionId)' didReceiveStream: *DataParty does not use this**")
    }
    
    // Finished receiving a resource from remote peer and saved the content
    // in a temporary location - the app is responsible for moving the file
    // to a permanent location within its sandbox.
    // NOT USED
    public func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?)
    {
        NSLog("%@", "DataParty (Session: '\(sessionId)') didFinishReceivingResourceWithName: *DataParty does not use this**")
    }
    
    // Start receiving a resource from remote peer.
    // NOT USED
    public func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress)
    {

        NSLog("%@", "DataParty (Session: '\(sessionId)') didStartReceivingResourceWithName: *DataParty does not use this**")
    }
    
    // Made first contact with peer and have identity information about the
    // remote peer (certificate may be nil).
    //@available(iOS 7.0, *)
    //optional public func session(session: MCSession, didReceiveCertificate certificate: [AnyObject]?, fromPeer peerID: MCPeerID, certificateHandler: (Bool) -> Void)
}