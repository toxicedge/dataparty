//
//  DataPartyMessage.swift
//  DataParty
//
//  Created by Chris on 2016-03-15.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation


// Mesage types
internal enum DataPartyMessageType: String {
    case DataInventory = "DataInventory"
    case DataInventoryRequest = "DataInventoryRequest"
    case Data = "Data"
    case DataPack = "DataPack"
    case DataRequest = "DataRequest"
}

// Error types
internal enum DataPartyErrorType: String {
    case None = "None"
    case DataUnavailable = "DataUnavailable"
}

// Payload struct to hold various message data (depending on message type)
internal struct DataPartyMessagePayload {
    var dataIdentifiers: Set<DataPartyDataIdentifier>?
    var data: DataPartyData?
    var dataPack: Set<DataPartyData>?
    var dataInventory: DataPartyDataInventory?
    var error: DataPartyErrorType = DataPartyErrorType.None
}




// Message structure Data Party uses to communicate with other peers
internal class DataPartyMessage: NSObject, NSCoding {

    // MARK: Properties
    
    private struct PropertyKey {
        static let messageType = "messageType"
        static let senderDeviceId = "senderDeviceId"
        static let senderUsername = "senderUsername"
        static let dataPartyVersionNumber = "dataPartyVersionNumber"
        static let dataPartyBuildNumber = "dataPartyBuildNumber"
        static let dataIdentifiers = "dataIdentifiers"
        static let dataInventory = "dataInventory"
        static let data = "data"
        static let dataPack = "dataPack"
        static let error = "error"
    }
    
    internal private(set) var messageType: DataPartyMessageType
    internal private(set) var senderDeviceId: String
    internal private(set) var senderUsername: String
    internal var payload = DataPartyMessagePayload()
    internal private(set) var dataPartyVersionNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
    internal private(set) var dataPartyBuildNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String
    
    // MARK: Constructor
    
    init(messageType: DataPartyMessageType) {
        self.messageType = messageType
        self.senderUsername = NSUserName()
        self.senderDeviceId = UIDevice.currentDevice().identifierForVendor!.UUIDString
    }
    
    // MARK: Encode/Decode
    
    internal required init(coder aDecoder: NSCoder){
        self.messageType = DataPartyMessageType(rawValue: (aDecoder.decodeObjectForKey(PropertyKey.messageType) as! String))!
        self.senderUsername = aDecoder.decodeObjectForKey(PropertyKey.senderUsername) as! String
        self.senderDeviceId = aDecoder.decodeObjectForKey(PropertyKey.senderDeviceId) as! String
        self.dataPartyVersionNumber = aDecoder.decodeObjectForKey(PropertyKey.dataPartyVersionNumber) as! String
        self.dataPartyBuildNumber = aDecoder.decodeObjectForKey(PropertyKey.dataPartyBuildNumber) as! String
        // Payload
        self.payload.dataIdentifiers = aDecoder.decodeObjectForKey(PropertyKey.dataIdentifiers) as? Set<DataPartyDataIdentifier>
        self.payload.data = aDecoder.decodeObjectForKey(PropertyKey.data) as? DataPartyData
        self.payload.dataPack = aDecoder.decodeObjectForKey(PropertyKey.dataPack) as? Set<DataPartyData>
        self.payload.dataInventory = aDecoder.decodeObjectForKey(PropertyKey.dataInventory) as? DataPartyDataInventory
        self.payload.error = DataPartyErrorType(rawValue: (aDecoder.decodeObjectForKey(PropertyKey.error) as! String))!
        
        super.init()
    }
    
    internal func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(self.messageType.rawValue, forKey: PropertyKey.messageType)
        aCoder.encodeObject(self.senderDeviceId, forKey: PropertyKey.senderDeviceId)
        aCoder.encodeObject(self.senderUsername, forKey: PropertyKey.senderUsername)
        aCoder.encodeObject(self.dataPartyVersionNumber, forKey: PropertyKey.dataPartyVersionNumber)
        aCoder.encodeObject(self.dataPartyBuildNumber, forKey: PropertyKey.dataPartyBuildNumber)
        // Payload
        aCoder.encodeObject(self.payload.dataIdentifiers, forKey: PropertyKey.dataIdentifiers)
        aCoder.encodeObject(self.payload.data, forKey: PropertyKey.data)
        aCoder.encodeObject(self.payload.dataPack, forKey: PropertyKey.dataPack)
        aCoder.encodeObject(self.payload.dataInventory, forKey: PropertyKey.dataInventory)
        aCoder.encodeObject(self.payload.error.rawValue, forKey: PropertyKey.error)
    }
}