//
//  DataPartyDataDetails.swift
//  DataParty
//
//  Created by Chris on 2016-03-06.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation

// A class for storing meta data for use in DPData objects
public class DataPartyDataDetails: NSObject, NSCoding {
    
    // MARK: NSCoding properties

    public struct PropertyKey {
        static let dateCreated = "dateCreated"
        static let creatorUsername = "creatorUserName"
        static let creatorDeviceId = "creatorDeviceId"
        static let dateReceived = "dateReceived"
        static let customAttributes = "customAttributes"
    }
    
    // MARK: Properties
    
    public var dateCreated: NSDate?
    public var creatorUsername: String?
    public var creatorDeviceId: String?
    public var dateReceived: NSDate?
    public var customAttributes: [String: String] // Dictionary for storing custom attributes

    
    // MARK: Constructor
    
    override init() {
        // defaults
        self.dateCreated = NSDate()
        self.creatorUsername = NSUserName()
        self.creatorDeviceId = UIDevice.currentDevice().identifierForVendor!.UUIDString
        self.customAttributes = [String: String]()
        super.init()
    }
    
    init(creatorDeviceId:String?, creatorUsername:String?, dateCreated:NSDate?, dateReceived:NSDate?, customAttributes:String?) {
        self.creatorDeviceId = creatorDeviceId
        self.creatorUsername = creatorUsername
        self.dateCreated = dateCreated
        self.dateReceived = dateReceived
        self.customAttributes = [String: String]()
        
        if (customAttributes != nil && customAttributes != "") {
            // convert custom attributes from String to [String: String]
            for entry in customAttributes!.componentsSeparatedByString(",") {
                let buffer = entry.componentsSeparatedByString(",")
                self.customAttributes[buffer[0]] = buffer[1]
            }
        }
    }
    
    // convert customAttributes dictionary into string of format key:value,key:value,...
    public func getCustomAttributesAsString() -> String? {
        if (customAttributes.count == 0) {
            return nil
        }
        
        var result = String()
        for (key,val) in customAttributes {
            result += key + ":" + val + ","
        }
        
        return String(result.characters.dropLast())
    }
    
    
    // MARK: Encode/Decode 

    public required convenience init(coder aDecoder: NSCoder){
        self.init()
        // override defaults
        self.dateCreated = aDecoder.decodeObjectForKey(PropertyKey.dateCreated) as? NSDate
        self.creatorUsername = aDecoder.decodeObjectForKey(PropertyKey.creatorUsername) as? String
        self.creatorDeviceId = aDecoder.decodeObjectForKey(PropertyKey.creatorDeviceId) as? String
        self.dateReceived = aDecoder.decodeObjectForKey(PropertyKey.dateReceived) as? NSDate
        if let tags = aDecoder.decodeObjectForKey(PropertyKey.customAttributes) as? [String: String] {
          self.customAttributes = tags // update only if not nil
        }
    }

    public func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(self.dateCreated, forKey: PropertyKey.dateCreated)
        aCoder.encodeObject(self.creatorUsername, forKey: PropertyKey.creatorUsername)
        aCoder.encodeObject(self.creatorDeviceId, forKey: PropertyKey.creatorDeviceId)
        aCoder.encodeObject(self.dateReceived, forKey: PropertyKey.dateReceived)
        aCoder.encodeObject(self.customAttributes, forKey: PropertyKey.customAttributes)
    }
}

