//
//  DataPartyDataRegistry.swift
//  DataParty
//
//  Created by Chris on 2016-03-06.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation
import CoreData

// Stores and retrieves information about data that has been used by DataParty
internal class DataPartyDataRegistry
{
    init(){}
}



extension DataPartyDataRegistry
{
    // MARK: Core Data
    static let coreDataStack = CoreDataStack()
    static let moc = coreDataStack.managedObjectContext
    
    // get raw DataDetails record from database (of type NSManagedObject)
    private static func getDataDetailsEntryByDataType(dataType:String, andDataHash dataHash:NSData) -> NSManagedObject? {
        let request = NSFetchRequest(entityName: DataDetailsTable.tableName)
        request.predicate = NSPredicate(format: "\(DataDetailsTable.fieldDataType) == %@ AND \(DataDetailsTable.fieldDataHash) == %@", dataType, dataHash)
        
        do {
            let results = try moc.executeFetchRequest(request) as! [NSManagedObject]
            if results.count > 0 {
                return results[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }

        return nil
    }
    
    // get raw entry of the IgnoreList table by dataType and dataHash
    private static func getIgnoreListEntryByDataType(dataType:String, andDataHash dataHash:NSData) -> NSManagedObject? {
        let request = NSFetchRequest(entityName: IgnoreListTable.tableName)
        request.predicate = NSPredicate(format: "\(IgnoreListTable.fieldDataType) == %@ AND \(IgnoreListTable.fieldDataHash) == %@", dataType, dataHash)
        
        do {
            let results = try moc.executeFetchRequest(request) as! [NSManagedObject]
            if results.count > 0 {
                return results[0]
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    
    
    // MARK: Convenience functions
    
    // Check if data object has been registered previously
    static func isDataRegistered(data: DataPartyData) -> Bool
    {
        return isDataRegistered(data.dataIdentifier)
    }

    // Remove data from ignore list using data object
    static func removeDataFromIgnoreList(data: DataPartyData)
    {
        removeDataFromIgnoreList(data.dataIdentifier);
    }
    
    // Get data details and insert them into the specified data
    static func injectDataDetails(data: DataPartyData)
    {
        // inject data details if it exists in the database
        let dataDetails = getDataDetails(data.dataIdentifier)
        if dataDetails != nil {
            data.dataDetails = dataDetails!
        }
    }
    
    // Check if data is on ignore list
    static func isDataIgnored(data: DataPartyData) -> Bool
    {
        return isDataIgnored(data.dataIdentifier)
    }
    
    // Add data to ignore list using data object
    static func addDataToIgnoreList(data: DataPartyData)
    {
        return addDataToIgnoreList(data.dataIdentifier)
    }
    
    // MARK: Implementations
    
    // Return data details object for specified data
    static func getDataDetails(dataId: DataPartyDataIdentifier) -> DataPartyDataDetails?
    {
        let optionalEntry = getDataDetailsEntryByDataType(dataId.dataType, andDataHash:dataId.dataHash)
        if (optionalEntry != nil) {
            let entry = optionalEntry!
            return DataPartyDataDetails(
                creatorDeviceId: entry.valueForKey(DataDetailsTable.fieldCreatorDeviceId) as? String,
                creatorUsername: entry.valueForKey(DataDetailsTable.fieldCreatorUsername) as? String,
                dateCreated: entry.valueForKey(DataDetailsTable.fieldDateCreated) as? NSDate,
                dateReceived: entry.valueForKey(DataDetailsTable.fieldDateReceived) as? NSDate,
                customAttributes: entry.valueForKey(DataDetailsTable.fieldCustomAttributes) as? String
            )
        }

        return nil
    }
    
    // Check if data has been registered using data identifier
    static func isDataRegistered(dataId: DataPartyDataIdentifier) -> Bool
    {
        if getDataDetailsEntryByDataType(dataId.dataType, andDataHash:dataId.dataHash) == nil {
            return false
        }
        else {
            return true
        }
    }
    
    // Register data
    static func registerData(data: DataPartyData)
    {
        // check if data metadata existed
        let key = data.dataIdentifier
        let optionalEntry = getDataDetailsEntryByDataType(key.dataType, andDataHash:key.dataHash)
        var entry:NSManagedObject
        
        // if data details does not exist, create new one
        if optionalEntry == nil {
            let entity =  NSEntityDescription.entityForName(DataDetailsTable.tableName, inManagedObjectContext:moc)!
            entry = NSManagedObject(entity: entity, insertIntoManagedObjectContext: moc)
            entry.setValue(key.dataType, forKey: DataDetailsTable.fieldDataType)
            entry.setValue(key.dataHash, forKey: DataDetailsTable.fieldDataHash)
        }
        else {
            entry = optionalEntry!
        }
        
        // set the all new values for the entry
        let metadata = data.dataDetails
        entry.setValue(metadata.creatorDeviceId, forKey: DataDetailsTable.fieldCreatorDeviceId)
        entry.setValue(metadata.creatorUsername, forKey: DataDetailsTable.fieldCreatorUsername)
        entry.setValue(metadata.dateCreated, forKey: DataDetailsTable.fieldDateCreated)
        entry.setValue(metadata.dateReceived, forKey: DataDetailsTable.fieldDateReceived)
        entry.setValue(metadata.getCustomAttributesAsString(), forKey: DataDetailsTable.fieldCustomAttributes)
        
        // persist the entry back to the database
        do {
            try moc.save()
        }
        catch let error as NSError  {
            print("Failed to save \(error), \(error.userInfo)")
        }
    }
    
    // Add data to ignore list using data identifier
    static func addDataToIgnoreList(dataIdentifier: DataPartyDataIdentifier)
    {
        // if data is not in the ignore list yet, add it
        if isDataIgnored(dataIdentifier) == false {
            let entity =  NSEntityDescription.entityForName(DataDetailsTable.tableName, inManagedObjectContext:moc)!
            let entry = NSManagedObject(entity: entity, insertIntoManagedObjectContext: moc)
            entry.setValue(dataIdentifier.dataType, forKey: DataDetailsTable.fieldDataType)
            entry.setValue(dataIdentifier.dataHash, forKey: DataDetailsTable.fieldDataHash)
            
            do {
                try moc.save()
            }
            catch let error as NSError {
                print("Failed to save \(error), \(error.userInfo)")
            }
        }
    }
    
    // Remove data from ignore list using data identifier
    static func removeDataFromIgnoreList(dataIdentifier: DataPartyDataIdentifier)
    {
        // if data is already in the IgnoreList, remove its
        let optionalEntry = getIgnoreListEntryByDataType(dataIdentifier.dataType, andDataHash: dataIdentifier.dataHash)
        if optionalEntry != nil {
            moc.deleteObject(optionalEntry!)
            do {
                try moc.save()
            }
            catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
    }
    
    // Check if data is ignored using data object
    static func isDataIgnored(dataIdentifier: DataPartyDataIdentifier) -> Bool
    {
        let request = NSFetchRequest(entityName: IgnoreListTable.tableName)
        request.predicate = NSPredicate(format: "\(IgnoreListTable.fieldDataType) == %@ AND \(IgnoreListTable.fieldDataHash) == %@", dataIdentifier.dataType, dataIdentifier.dataHash)
        
        let error = NSErrorPointer()
        if moc.countForFetchRequest(request, error: error) > 0 {
            return true
        }
        else {
            return false
        }
    }
}
