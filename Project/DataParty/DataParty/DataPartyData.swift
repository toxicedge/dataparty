//
//  DataPartyData.swift
//  DataParty
//
//  Created by Chris on 2016-02-23.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation
import CryptoSwift

// A class to encapsulate data objects to be synchronized between devices
public class DataPartyData: NSObject, NSCoding {
    
    // MARK: Properties
    
    private struct PropertyKey {
        static let data = "data"
        static let dataIdentifier = "dataIdentifier"
        static let dataDetails = "dataDetails"
    }
    
    public let data: NSData
    internal let dataIdentifier: DataPartyDataIdentifier
    public internal(set) var dataDetails: DataPartyDataDetails
    public var disableSync = false
    
    // Helper properties
    public var dataType: String {
        get {
            return dataIdentifier.dataType
        }
    }
    public var dataHash: NSData {
        get {
            return dataIdentifier.dataHash
        }
    }
    
    // MARK: Constructor
    
    // Constructor
    public convenience init (dataType: String, data: NSData) {
        self.init(dataType: dataType, data: data, dataHash: DataPartyData.generateHash(data))
    }
    
    // Constructor allowing custom unique ID
    public init (dataType: String, data: NSData, dataHash: NSData) {
        self.data = data
        self.dataIdentifier = DataPartyDataIdentifier(dataType: dataType, dataHash: dataHash)
        self.dataDetails = DataPartyDataDetails()
        super.init()
    }
    
    // MARK: Encode/Decode
    
    public required init(coder aDecoder: NSCoder){
        self.data = aDecoder.decodeObjectForKey(PropertyKey.data) as! NSData
        self.dataIdentifier = aDecoder.decodeObjectForKey(PropertyKey.dataIdentifier) as! DataPartyDataIdentifier
        self.dataDetails = aDecoder.decodeObjectForKey(PropertyKey.dataDetails) as! DataPartyDataDetails
        super.init()
    }
    
    public func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(self.data, forKey: PropertyKey.data)
        aCoder.encodeObject(self.dataIdentifier, forKey: PropertyKey.dataIdentifier)
        aCoder.encodeObject(self.dataDetails, forKey: PropertyKey.dataDetails)
    }
    
    // Default data hash generation function using MD5 algorythm
    public static func generateHash(data: NSData) -> NSData {
        return data.md5()
    }
    
    // Comparison
    override public func isEqual(object: AnyObject?) -> Bool
    {
        var result = false;
        
        if let object = object as? DataPartyData
        {
            if self == object
            {
                result = true
            }
            else if self.dataIdentifier == object.dataIdentifier
            {
                result = true
            }
        }

        return result
    }
    
    override public var hash: Int
    {
        return self.dataIdentifier.hash
    }
}



// Comparison

func ==(left: DataPartyData, right: DataPartyData) -> Bool
{
    return left.dataIdentifier == right.dataIdentifier
}


