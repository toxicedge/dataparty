//
//  DataPartyInvitationMessage.swift
//  DataParty
//
//  Created by Chris on 2016-04-04.
//  Copyright © 2016 Toxicedge. All rights reserved.
//

import Foundation
import MultipeerConnectivity

internal class DataPartyInvitationMessage: NSObject, NSCoding {
    
    private struct PropertyKey {
        static let deviceId = "deviceId"
        static let connectedPeers = "connectedPeers"
    }
    
    var deviceId: String
    var connectedPeersCount: Int
    {
        get
        {
            return connectedPeers.count
        }
    }
    var connectedPeers = Set<MCPeerID>()
    
    init(connectedPeers: [MCPeerID])
    {
        self.deviceId = UIDevice.currentDevice().identifierForVendor!.UUIDString
        
        super.init()
        
        connectedPeers.forEach({(peer) in
            self.connectedPeers.insert(peer)
        })
    }
    
    internal required init(coder aDecoder: NSCoder){
        self.deviceId = aDecoder.decodeObjectForKey(PropertyKey.deviceId) as! String
        self.connectedPeers = aDecoder.decodeObjectForKey(PropertyKey.connectedPeers) as! Set<MCPeerID>
        super.init()
    }
    
    internal func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(self.deviceId, forKey: PropertyKey.deviceId)
        aCoder.encodeObject(self.connectedPeers, forKey: PropertyKey.connectedPeers)
    }
}